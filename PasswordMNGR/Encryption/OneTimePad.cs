﻿using System;
using System.Numerics;

namespace PasswordMNGR.Encryption
{
    public class OneTimePad : IEncryption
    {
        public byte[] Encrypt(byte[] data, string seed)
        {
            return Run(data, seed);
        }

        public byte[] Decrypt(byte[] data, string seed)
        {
            return Run(data, seed);
        }

        private byte[] Run(byte[] data, string seed)
        {
            var key = CreateOneTimePad(data, seed);
            return XorByteArrays(data, key);
        }

        private byte[] CreateOneTimePad(byte[] bytes, string str)
        {
            var strBytes = new byte[str.Length * sizeof(char)];
            Buffer.BlockCopy(str.ToCharArray(), 0, strBytes, 0, strBytes.Length);
            var chur = new byte[bytes.Length + 1];
            bytes.CopyTo(chur, 0);
            chur[chur.Length - 1] = Convert.ToByte("0");
            var fileValue = new BigInteger(chur);
            var passwordValue = new BigInteger(strBytes);
            while (passwordValue < fileValue)
                passwordValue *= passwordValue;
            return passwordValue.ToByteArray();
        }

        private byte[] XorByteArrays(byte[] filesBytes, byte[] key)
        {
            var combinedBytes = new byte[filesBytes.Length];
            for (var i = 0; i < filesBytes.Length; i++)
                combinedBytes[i] = (byte)(filesBytes[i] ^ key[i]);
            return combinedBytes;
        }
    }
}