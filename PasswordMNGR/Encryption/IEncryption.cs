﻿namespace PasswordMNGR.Encryption
{
    public interface IEncryption
    {
        byte[] Encrypt(byte[] data, string seed);
        byte[] Decrypt(byte[] data, string seed);
    }
}