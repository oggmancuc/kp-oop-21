﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace PasswordMNGR
{
    [Serializable]
    public class Account : IComparable, INotifyPropertyChanged
    {
        private string title;
        private string userName;
        private string email;
        private string password;
        private string description;

        public string Title
        {
            get { return title; }
            set { title = value; NotifyPropertyChanged(nameof(Title)); }
        }

        public string UserName
        {
            get { return userName; }
            set { userName = value; NotifyPropertyChanged(nameof(UserName)); }
        }

        public string Email
        {
            get { return email; }
            set { email = value; NotifyPropertyChanged(nameof(Email)); }
        }

        public string Password
        {
            get { return password; }
            set { password = value; NotifyPropertyChanged(nameof(Password)); }
        }

        public string Description
        {
            get { return description; }
            set { description = value; NotifyPropertyChanged(nameof(Description)); }
        }

        public int CompareTo(object obj)
        {
            if (obj == null) 
                return 1;
            if (!(obj is Account otherEntry))
                throw new ArgumentException("Об'єкт не є аккаунтом");
            return string.Compare(Title, otherEntry.Title, StringComparison.OrdinalIgnoreCase);
        }

        [field: NonSerialized]
        public event PropertyChangedEventHandler PropertyChanged;
        protected virtual void NotifyPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}