﻿using Microsoft.Win32;
using PasswordMNGR.Encryption;
using PasswordMNGR.Properties;
using System;
using System.ComponentModel;
using System.IO;
using System.Runtime.CompilerServices;
using System.Runtime.Serialization.Formatters.Binary;
using System.Windows;
using System.Windows.Data;

namespace PasswordMNGR
{
    public partial class MainWindow : INotifyPropertyChanged
    {
        private readonly IEncryption Encrypter;
        private readonly CollectionView View;
        public Collection<Account> Accounts { get; set; } = new Collection<Account>();
        public string Password { get; set; }

        public string FileLocation
        {
            get { return Settings.Default.FileLocation; }
            set
            {
                Settings.Default.FileLocation = value;
                Settings.Default.Save();
                FileOpen = false;
                ClearAllForms();
                NotifyPropertyChanged(nameof(FileName));
            }
        }

        public string FileName
        {
            get { return string.IsNullOrEmpty(FileLocation) ? "файл не обрано" : FileLocation.Substring(FileLocation.LastIndexOf("\\") + 1, FileLocation.LastIndexOf(".") - 1 - FileLocation.LastIndexOf("\\")); }
        }

        private bool fileOpen;

        public bool FileOpen
        {
            get { return fileOpen; }
            set
            {
                fileOpen = value;
                NotifyPropertyChanged(nameof(FileOpen));
            }
        }

        private string searchText;

        public string SearchText
        {
            get { return searchText; }
            set
            {
                searchText = value;
                View.Refresh();
            }
        }

        private Account selectedAcc;

        public Account SelectedAcc
        {
            get { return selectedAcc; }
            set
            {
                selectedAcc = value;
                IsAccSelected = value != null;
                NotifyPropertyChanged(nameof(SelectedAcc));
            }
        }

        private bool isAccSelected;

        public bool IsAccSelected
        {
            get { return isAccSelected; }
            set
            {
                isAccSelected = value;
                NotifyPropertyChanged(nameof(IsAccSelected));
            }
        }

        public MainWindow()
        {
            InitializeComponent();
            View = (CollectionView)CollectionViewSource.GetDefaultView(Accounts);
            View.Filter = SearchFilter;
            Encrypter = new OneTimePad();
            textBoxKey.Focus();
        }

        private bool SearchFilter(object item)
        {
            if (string.IsNullOrEmpty(SearchText)) return true;
            return ((Account)item).Title.IndexOf(SearchText, StringComparison.OrdinalIgnoreCase) >= 0;
        }

        private void ClearAllForms()
        {
            Accounts.Clear();
            Password = string.Empty;
            SearchText = string.Empty;
        }

        private void buttonOpen_Click(object sender, RoutedEventArgs e)
        {
            var openFileDialog = new OpenFileDialog
            {
                Title = "Оберіть зашифрований файл",
                Filter = "Зашифрований файл (*.edf)|*.edf"
            };
            var result = openFileDialog.ShowDialog();
            if (result == true)
            {
                FileLocation = openFileDialog.FileName;
                FileOpen = false;
            }
        }

        private void buttonCreate_Click(object sender, RoutedEventArgs e)
        {
            var saveFileDialog = new SaveFileDialog
            {
                Title = "Збережіть зашифрований файл",
                Filter = "Зашифрований файл (*.edf)|*.edf"
            };
            var result = saveFileDialog.ShowDialog();
            if (result == true)
            {
                FileLocation = saveFileDialog.FileName;
                FileOpen = true;
            }
        }

        private bool CheckInputFields()
        {
            var valid = true;
            if (string.IsNullOrEmpty(FileLocation))
            {
                valid = false;
            }
            if (string.IsNullOrEmpty(Password))
            {
                valid = false;
            }
            return valid;
        }

        private void buttonEncrypt_Click(object sender, RoutedEventArgs e)
        {
            if (!CheckInputFields()) return;

            var binaryFormatter = new BinaryFormatter();
            var memoryStream = new MemoryStream();
            binaryFormatter.Serialize(memoryStream, Accounts);
            var decryptedBytes = memoryStream.ToArray();
            var encryptedBytes = Encrypter.Encrypt(decryptedBytes, Password);
            using (var stream = File.Open(FileLocation, FileMode.Create))
                stream.Write(encryptedBytes, 0, encryptedBytes.Length);
        }

        private void buttonDecrypt_Click(object sender, RoutedEventArgs e)
        {
            if (!CheckInputFields()) return;
            if (!File.Exists(FileLocation)) return;

            var encryptedBytes = File.ReadAllBytes(FileLocation);
            var decryptedBytes = Encrypter.Decrypt(encryptedBytes, Password);
            var memoryStream = new MemoryStream();
            var binaryFormatter = new BinaryFormatter();
            memoryStream.Write(decryptedBytes, 0, decryptedBytes.Length);
            memoryStream.Position = 0;
            var deserialised = (Collection<Account>)binaryFormatter.Deserialize(memoryStream);
            Accounts.Clear();
            Accounts.AddAll(deserialised);
            FileOpen = true;
        }

        private void buttonAddA_Click(object sender, RoutedEventArgs e)
        {
            var newEntry = new Account { Title = "Новий аккаунт" };
            Accounts.Add(newEntry);
            listBoxAccs.SelectedIndex = Accounts.IndexOf(newEntry);
        }

        private void buttonDelA_Click(object sender, RoutedEventArgs e)
        {
            if (!(listBoxAccs.SelectedItem is Account selectedItem)) return;
            var index = Accounts.IndexOf(selectedItem);
            Accounts.Remove(selectedItem);
            listBoxAccs.SelectedIndex = Math.Min(index, Accounts.Count - 1);
        }

        public event PropertyChangedEventHandler PropertyChanged;
        protected virtual void NotifyPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}